﻿namespace server.api.Controllers
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using server.services.AzureBlobStorageEnum;
    using server.services.AzureStorage;
    using server.services.Response;

    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class ValuesController : ControllerBase
    {
        private readonly IAzureStorageService<AzureResponse, IFormFile> _storageBlob;

        public ValuesController(IAzureStorageService<AzureResponse, IFormFile> storageBlob)
        {
            _storageBlob = storageBlob;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            _storageBlob.CreateOrGetAzureBlob(AzureBlobStorageEnum.ImagesContainer);
            return Ok(_storageBlob.BlobList());
        }

        [HttpPost]
        public ActionResult UploadImage([FromForm]List<IFormFile> files)
        {
            AzureResponse response = new AzureResponse();
            var files1 = HttpContext.Request.Form.Files;
            // full path to file in temp location

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    _storageBlob.CreateOrGetAzureBlob(AzureBlobStorageEnum.ImagesContainer);
                    response = _storageBlob.UploadFile(formFile);
                }
            }
            if (response.IsUploaded)
                return Ok();
            else
                return NotFound();

        }

        [HttpDelete("{id}")]
        public ActionResult<string> Delete(string id)
        {
            _storageBlob.CreateOrGetAzureBlob(AzureBlobStorageEnum.ImagesContainer);

            var isDeleted = _storageBlob.DeleteBlob(id);
            if (isDeleted)
                return Ok();
            else
                return NotFound();
        }

        [HttpGet("{id}")]
        public ActionResult Download(string id)
        {
            _storageBlob.CreateOrGetAzureBlob(AzureBlobStorageEnum.ImagesContainer);
            var stream = _storageBlob.DownloadBlob(id);
            return File(stream, "image/png", id);
        }

        [HttpGet("deleteall")]
        public ActionResult DeleteAll()
        {
            _storageBlob.CreateOrGetAzureBlob(AzureBlobStorageEnum.ImagesContainer);
            _storageBlob.DeleteAll();
            return Ok();
        }
    }
}
