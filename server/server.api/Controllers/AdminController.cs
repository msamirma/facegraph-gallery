
namespace server.api.Controllers
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using server.services.AzureBlobStorageEnum;
    using server.services.AzureStorage;
    using server.services.Response;

    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAzureStorageService<AzureResponse, IFormFile> _storageBlob;

        public AdminController(IAzureStorageService<AzureResponse, IFormFile> storageBlob)
        {
            _storageBlob = storageBlob;
        }
        [HttpDelete("{pin}")]
        public ActionResult DeleteAll(string pin)
        {
            if (pin == "1234")
            {
                _storageBlob.CreateOrGetAzureBlob(AzureBlobStorageEnum.ImagesContainer);
                _storageBlob.DeleteAll();
                return Ok();
            }
            return NotFound();
        }
    }
}