﻿namespace server.services.FileUploadService
{
    using server.services.Response;

    public interface IFileUpload<T, U> where T : BaseResponse where U : class
    {
        T UploadFile(U FileToUpload);
    }
}
