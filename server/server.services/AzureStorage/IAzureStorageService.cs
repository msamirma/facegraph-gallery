﻿

namespace server.services.AzureStorage
{
    using System.Web;
    using System.Collections.Generic;
    using server.services.Response;
    using server.services.FileUploadService;
    using System.IO;

    public interface IAzureStorageService<T, U> : IFileUpload<T, U> where T : BaseResponse where U : class
    {
        void CreateOrGetAzureBlob(string ContainerName);
        List<string> BlobList();
        bool DeleteBlob(string AbsoluteUri);
        Stream DownloadBlob(string AbsoluteUri);
        void DeleteAll();
    }
}
