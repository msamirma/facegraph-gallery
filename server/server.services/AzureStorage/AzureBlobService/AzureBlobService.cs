﻿
namespace server.services.BlobService
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.Azure.Storage;
    using Microsoft.Azure.Storage.Blob;
    using Microsoft.Extensions.Configuration;
    using server.services.AzureStorage;
    using server.services.Response;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class AzureBlobService : IAzureStorageService<AzureResponse, IFormFile>
    {
        private CloudBlobContainer blobContainer;
        public AzureBlobService(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void CreateOrGetAzureBlob(string ContainerName)
        {
            // var configuration = new ConfigurationBuilder();

            // Check if Container Name is null or empty  
            if (string.IsNullOrEmpty(ContainerName))
            {
                throw new ArgumentNullException(ContainerName, "Container Name can't be empty");
            }
            try
            {
                // Get azure table storage connection string.  
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Configuration["ConnectionString"]);

                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
                blobContainer = cloudBlobClient.GetContainerReference(ContainerName);

                // Create the container and set the permission  
                if (blobContainer.CreateIfNotExists())
                {
                    blobContainer.SetPermissions(
                        new BlobContainerPermissions
                        {
                            PublicAccess = BlobContainerPublicAccessType.Container
                        }
                    );
                }
            }
            catch (Exception ExceptionObj)
            {
                throw ExceptionObj;
            }
        }

        public AzureResponse UploadFile(IFormFile fileToUpload)
        {
            AzureResponse response = new AzureResponse();

            // Check HttpPostedFileBase is null or not  
            if (fileToUpload == null || fileToUpload.Length == 0)
                return null;
            try
            {
                // string fileName = Path.GetFileName(fileToUpload.FileName);
                var fileExt = System.IO.Path.GetExtension(fileToUpload.FileName).Substring(1);
                string fileName = DateTime.UtcNow.Ticks + "." + fileExt;
                CloudBlockBlob blockBlob;
                // Create a block blob  
                blockBlob = blobContainer.GetBlockBlobReference($"{fileName}");
                // Set the object's content type  
                blockBlob.Properties.ContentType = fileToUpload.ContentType;
                // upload to blob  
                blockBlob.UploadFromStream(fileToUpload.OpenReadStream());

                // get file uri  
                //response.FileUrl = blockBlob.Uri.AbsoluteUri;

                //check if file exists
                response.IsUploaded = blockBlob.Exists();

            }
            catch (Exception ExceptionObj)
            {
                throw ExceptionObj;
            }
            finally
            {
                fileToUpload.OpenReadStream().Dispose();
            }
            return response;
        }

        public bool DeleteBlob(string url)
        {
            string BaseUrl = Configuration["BaseUrl"];

            try
            {
                Uri uriObj = new Uri(BaseUrl + url);
                string BlobName = Path.GetFileName(uriObj.LocalPath);

                // get block blob refarence  
                CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(BlobName);

                // delete blob from container      
                blockBlob.Delete();
                return true;
            }
            catch (Exception ExceptionObj)
            {
                throw ExceptionObj;
            }
        }

        public List<string> BlobList()
        {
            List<string> _blobList = new List<string>();

            try
            {

                var results = blobContainer.ListBlobsSegmented(null);

                foreach (IListBlobItem item in results.Results)
                {
                    _blobList.Add(item.Uri.AbsoluteUri.ToString());
                }
                return _blobList;
            }
            catch (Exception ex)
            {
                return _blobList;
            }
        }

        public Stream DownloadBlob(string url)
        {
            string BaseUrl = Configuration["BaseUrl"];
            try
            {
                // string AbsoluteUri = $"{Configuration["BaseUrl"]}{id}";
                Uri uriObj = new Uri(BaseUrl + url);
                // Uri uriObj = new Uri(url);
                string BlobName = Path.GetFileName(uriObj.LocalPath);

                // get block blob refarence  
                CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(BlobName);

                blockBlob.DownloadToFile(url, FileMode.Create);
                var stream = blockBlob.OpenRead();
                // return File(stream, blockBlob.Properties.ContentType, "person.png");
                return stream;
            }
            catch (Exception ExceptionObj)
            {
                throw ExceptionObj;
            }
        }

        public void DeleteAll()
        {
            try
            {
                blobContainer.DeleteIfExists();
            }
            catch (Exception ex)
            {
                //ex
            }
        }
    }

}
