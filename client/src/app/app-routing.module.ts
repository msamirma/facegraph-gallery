import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { List } from './screens/List/List';
import { AddComponent } from './screens/add/add.component';


const routes: Routes = [
  { path: '', component: List },
  { path: 'add', component: AddComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
