import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpEventType } from '@angular/common/http';
// import { Observable } from 'rxjs';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { GalleryItem } from '../dto/gallery-item-dto';
const url = 'https://localhost:5001/api/values';
const adminUrl = 'https://localhost:5001/api/admin';
const imageUrlPath = 'https://fgtest2.blob.core.windows.net/images/';
@Injectable()
export class GalleryService {
    constructor(private httpClient: HttpClient) { }

    Get(): Observable<string[]> {
        return this.httpClient.get<string[]>(url).pipe(
            tap(data => {
                console.log('All: ' + JSON.stringify(data));
            }),
            catchError(this.handleError)
        );
    }

    Remove(item: string): void {
        const name = item.replace(imageUrlPath, '');

        this.httpClient.delete<boolean>(`${url}/${name}`)
            .subscribe(res => console.log(res));

    }

    Download(item: string): any {
        const name = item.replace(imageUrlPath, '');
        const extension = item.slice((item.lastIndexOf(".") - 1 >>> 0) + 2);

        this.httpClient.get(`${url}/${name}`, { responseType: 'arraybuffer' })
            .subscribe((res) => {
                this.writeContents(res, name, extension); // file extension
            });
    }

    deleteAll(pin: string): Observable<any> {
        return this.httpClient.delete<boolean>(`${adminUrl}/${pin}`);
    }

    Upload(data: any) {
        return this.httpClient.post<any>(url, data, {
            reportProgress: true,
            observe: 'events'
        }).pipe(map((event) => {
            switch (event.type) {

                case HttpEventType.UploadProgress:
                    const progress = Math.round(100 * event.loaded / event.total);
                    return { status: 'progress', message: progress };

                case HttpEventType.Response:
                    return event.body;
                default:
                    return `Unhandled event: ${event.type}`;
            }
        })
        );
    }

    private handleError(err: HttpErrorResponse) {

        let errorMessage = '';
        if (err.error instanceof ErrorEvent) {

            errorMessage = `An error occurred: ${err.error.message}`;
        } else {

            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }

    writeContents(content, fileName, contentType) {
        const a = document.createElement('a');
        const file = new Blob([content], { type: contentType });
        a.href = URL.createObjectURL(file);
        a.download = fileName;
        a.click();
    }
}
