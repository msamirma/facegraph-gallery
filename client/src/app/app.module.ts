import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { List } from './screens/List/List';
import { GalleryService } from './api-proxy/services/gallery-service';
import { MyMaterialModule } from './material.module';
import { ListItemComponent } from './Components/ListItem';
import { ModalComponent } from './Components/modal/modal.component';
import { AddComponent } from './screens/add/add.component';
@NgModule({
  declarations: [
    AppComponent,
    List,
    ListItemComponent,
    ModalComponent,
    AddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MyMaterialModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [GalleryService],
  bootstrap: [AppComponent, ModalComponent]
})
export class AppModule { }
