import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GalleryService } from '../api-proxy/services/gallery-service';

@Component({
    selector: 'img-item',
    templateUrl: './ListItem.html',
    styleUrls: ['ListItem.sass']

})
export class ListItemComponent {
    @Output() myEvent = new EventEmitter();
    @Input() image: string;

    constructor(private galleryService: GalleryService) {
    }

    onDownload(url) {
        this.galleryService.Download(url);
    }

    handleDelte(url) {
        this.myEvent.emit(url)
    }

}
