import { Component } from '@angular/core';
import { GalleryService } from '../../api-proxy/services/gallery-service';
import { GalleryItem } from '../../api-proxy/dto/gallery-item-dto';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
import { ModalComponent } from 'src/app/Components/modal/modal.component';

@Component({
    selector: 'List',
    templateUrl: 'List.html'
})
export class List {
    // galleryItems$: Observable<string[]>;
    galleryItems = new Array<string>();
    pin: string;
    constructor(private galleryService: GalleryService, public dialog: MatDialog) {
    }

    ngOnInit() {
        this.galleryService.Get().subscribe(res => {
            this.galleryItems = res.map(item => {
                return item;
            });
        });
    }

    onDelte($event) {
        this.galleryService.Remove($event);
        this.galleryItems = this.galleryItems.filter(item => item !== $event);
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(ModalComponent, {
            width: '250px',
            data: { pin: '' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.galleryService.deleteAll(result)
                    .subscribe(res => {
                        this.galleryItems = [];
                    }, err => {
                        console.log('Do nothing', err);

                    });
            }
        });
    }

}
