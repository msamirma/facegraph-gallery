import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GalleryService } from 'src/app/api-proxy/services/gallery-service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  form: FormGroup;
  error: string;
  uploadResponse = { status: '', message: '', filePath: '' };
  constructor(private formBuilder: FormBuilder, private galleryService: GalleryService, private router: Router) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      image: ['']
    });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('image').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('files', this.form.get('image').value);

    this.galleryService.Upload(formData).subscribe(
      (res) => {
        if (res && res.status) {
          this.uploadResponse = res;
        }
        setTimeout(() => {
          if (res && res.message && res.message == '100')
            this.router.navigate(['/']);
        }, 3000);

      },
      (err) => {
        this.error = err
      }
    );
  }

}
