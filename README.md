# facegraph-gallery

FaceGraph code challenge

### # Steps to run the solution

1. Clone the repo.
2. open terminal then type `$ cd server` then `$ cd server.api`
3. then type `$ dotnet restore`.
4. then type `$ dotnet run`.
5. new terminal and type `$ cd client` then `$ npm i`
6. `$ ng serve -o`

### # Server

##### Folder Stucture

- Api porject is classic dotnet core api.
- services project contains azure blob storage implemnation and could be extended to handle any other blob provider like AWS.

### # Client

##### Folder Stucture

- Angular project using angular CLI , bootstrap and angular material.

1. api-proxy contains services and dto responsible for handling api calls.
2. Components contains all screens components.
3. Screens contains two screens for list and add.

### # Admin auth

**I made it very simple as admin can authenticate with simple pin.**
